<?php 
require_once("Animal.php");
require_once("Frog.php");
require_once("Ape.php");

$sheep = new Animal("Shaun");
echo "$sheep->name <br>";
echo "$sheep->legs <br>";
echo "$sheep->cold_blooded <br><br>";

$kodok = new Frog("buduk");
echo "$kodok->name <br>";
echo "$kodok->legs <br>";
echo "$kodok->cold_blooded <br>";
echo $kodok->jump()."<br><br>";

$kera = new Ape("Sungokong");
echo "$kera->name <br>";
echo "$kera->legs <br>";
echo "$kera->cold_blooded <br>";
echo $kera->yell()."<br><br>";


?>